// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyAGqlosjsa61jyMJ6jTXYs-KzNfpVCPW1Y",
    authDomain: "bbbc-3c61a.firebaseapp.com",
    databaseURL: "https://bbbc-3c61a.firebaseio.com",
    projectId: "bbbc-3c61a",
    storageBucket: "bbbc-3c61a.appspot.com",
    messagingSenderId: "847529540808",
    appId: "1:847529540808:web:71e3f3109f80d8983cf9fd",
    measurementId: "G-KD3Z3RVV45"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

export interface User {
    // Qustion mark field is not required
    uid:string,
    email?: string | null, 
    photoUrl?: string,
    displayName?:string
 }
 
 
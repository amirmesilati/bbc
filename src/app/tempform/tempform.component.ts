import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tempform',
  templateUrl: './tempform.component.html',
  styleUrls: ['./tempform.component.css']
})
export class TempformComponent implements OnInit {

  cities:object[] = [{id:1, name:'Tel Aviv'},{id:1, name:'non-exitent'},{id:1, name:'London'},{id:1, name:'Paris'},{id:1, name:'New York'},{id:1, name:'Abu Dhabi'},{id:1, name:'Las Vegas'},{id:1, name:'Eilat'},{id:1, name:'Berlin'},{id:1, name:'Rome'},{id:1, name:'Hawai'},{id:1, name:'Colorado'}];
  temperature:number;
  city:string; 

  onSubmit(){
    this.router.navigate(['/temperatures',this.city]);
  }
  
  constructor(private router: Router) { }

  ngOnInit() {
  }

}

import { Weather } from './../interfaces/weather';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { TempService } from '../temp.service';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {
likes:number= 0;
temperature:number; 
city:String;
image:String;
errorMessage:string; 
hasError:boolean = false; 


tempData$:Observable<Weather>; //OBSERVABLES MARKED WITH DOLLAR SIGN$$$$

addLikes(){
  this.likes++  //// the likes counter at temperatures
}

  constructor(private route: ActivatedRoute, private tempService:TempService) { }

  ngOnInit() {
    //this.temperature = this.route.snapshot.params.temp;
    this.city= this.route.snapshot.params.city;
    this.tempData$ = this.tempService.searchWeatherData(this.city);
    this.tempData$.subscribe(
      data => {
        console.log(data);
        this.temperature = data.temperature;
        this.image = data.image;
      } ,
      error =>{
        console.log("in the component" + error.message);
        this.hasError = true; 
        this.errorMessage = error.message; 
      } 

    
    )
    }
  }
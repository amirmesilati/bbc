import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bookform',
  templateUrl: './bookform.component.html',
  styleUrls: ['./bookform.component.css']
})
export class BookformComponent implements OnInit {

  constructor(private booksService:BooksService,
              public authService:AuthService, 
              private router:Router,
              private route: ActivatedRoute) { }

  title:string;
  author:string; 
  id:string;
  userId:string;
  isEdit:boolean = false;
  buttonText:string = 'Add book' 
  
  onSubmit(){ 
    if(this.isEdit){
      console.log('edit mode');
      this.booksService.updateBook(this.userId, this.id,this.title,this.author);
    } else {
      console.log('In onSubmit');
      this.booksService.addBook(this.userId,this.title,this.author)
    }
    this.router.navigate(['/books']);  
  }  

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;    
        if(this.id) {
          this.isEdit = true;
          this.buttonText = 'Update book'
          this.booksService.getBook(this.userId,this.id).subscribe(
          book => {
            console.log(book.data().author)
            console.log(book.data().title)
            this.author = book.data().author;
            this.title = book.data().title;})        
      }
   })
  }
}
